
import {types} from '../types/ActionTypes'

const initialStateShoppingCart : any = {
  response: [],
  loading: false,
  errors: {},
  fields:{},
  message:{},
  data_cart:[]
};

export const getDataShoppingCart = ( state = initialStateShoppingCart, action: any ) : any => {

  switch (action.type) {
    case types.LOADING_CHANGE_SHOPPINGCART:
      return {
        ...state,
        loading:!state.loading
      };
    case types.SET_DATA_SHOPPINGCART:
      
      return {
        ...state,
        [action.name]: action.items
      };
    default:
      return state;
  }
  
};