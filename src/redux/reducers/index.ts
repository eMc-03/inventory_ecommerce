import { combineReducers } from 'redux';
import { getDataHome } from './HomeReducer';
import { getDataShoppingCart } from './ShoppingCartReducer';

const rootReducer = combineReducers({
  home: getDataHome,
  ShoppingCart: getDataShoppingCart
});

export default rootReducer;