
import {types} from '../types/ActionTypes'

const initialStateCourse : any = {
  response: [],
  loading: false,
  errors: {},
  fields:{},
  message:{},
  categories:[]
  
};

export const getDataHome = ( state = initialStateCourse, action: any ) : any => {

  switch (action.type) {
    case types.LOADING_CHANGE_HOME:
      return {
        ...state,
        loading:!state.loading
      };
    case types.SET_DATA_HOME:
      
      return {
        ...state,
        [action.name]: action.items
      };
    default:
      return state;
  }
  
};