import { getRequest, postRequest, putRequest } from './http'
import { types } from '../types/ActionTypes'


export function loading() {
  return {
    type: types.LOADING_CHANGE_SHOPPINGCART
  }
}

export function setData(items: any, name: any) {
  return {
    type: types.SET_DATA_SHOPPINGCART,
    name: name,
    items: items
  }
}

const _get = (data: any, controller: any) => {
  return (dispatch: any) => {
    dispatch(loading());
    getRequest(data, controller).then(
      (response) => {
        if (data.typeOption == "get-data") {
          dispatch(setData(response.data.data.categories, 'categories'));
        }
      },
      (err) => {

      }
    );
  }
};


const _post = (data: any, controller: any) => {
  return (dispatch : any) => {
      dispatch(loading());
      postRequest(data, controller).then(
          (response) => {

          },
          (error) => {
            
          }
      );
  }
};

export default {
  loading,
  setData,
  _post
}