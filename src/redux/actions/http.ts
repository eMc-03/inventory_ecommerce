import axios from 'axios'
import { API } from '../utils/constants'
import { getDataJWT } from '../utils/functions';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'

export function getRequest(data: any, view: any) {
  return axios.get(`${API}${view}`, { params: data, headers: { Authorization: "Bearer " + getDataJWT() } });
}

export function postRequest(data: any, view: any) {
  return axios.post(`${API}${view}`, data, { headers: { Authorization: "Bearer " + getDataJWT() } });
}

export function putRequest(data: any, view: any) {
  return axios.put(`${API}${view}`, data, { headers: { Authorization: "Bearer " + getDataJWT() } });
}