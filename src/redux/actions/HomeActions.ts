import { getRequest, postRequest, putRequest } from './http'
import { types } from '../types/ActionTypes'


export function loading() {
  return {
    type: types.LOADING_CHANGE_HOME
  }
}

export function setData(items: any, name: any) {
  return {
    type: types.SET_DATA_HOME,
    name: name,
    items: items
  }
}

const _get = (data: any, controller: any) => {
  return (dispatch: any) => {
    dispatch(loading());
    getRequest(data, controller).then(
      (response) => {
        if (data.typeOption == "get-data") {
          dispatch(setData(response.data.data.categories, 'categories'));
        }
      },
      (err) => {

      }
    );
  }
};


const _post = (data: any, controller: any) => {
  return (dispatch : any) => {
      dispatch(loading());
      postRequest(data, controller).then(
          (response) => {

              /*if (data.typeOption == "create_dic") {

                  //dispatch(setData({ message: response.data.message, type: 'success', id: Math.random() }, 'message'));
                  dispatch(setData(response.data, 'response'));
                  dispatch(setData(response.data.data, 'data'));

              }*/

          },
          (error) => {
              /*if (error.response.status === 500) {

                  dispatch(setData({ message: "Error en la Petición", type: 'error', id: Math.random() }, 'message'));

              } else if (error.response.status === 500) {

                  dispatch(setData({ message: "Error en la Petición", type: 'error', id: Math.random() }, 'message'));

              } else if (error.response.status === 401) {

                  dispatch(setData({ message: error.response.data.message, type: 'error', id: Math.random() }, 'message'));

              } else if (error.response.status === 401) {

                  dispatch(setData({ message: error.response.data.message, type: 'error', id: Math.random() }, 'message'));

              }*/
          }
      );
  }
};

export default {
  loading,
  _get,
  setData,
  _post
}