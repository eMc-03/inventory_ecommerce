import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import HomeActions from '../../redux/actions/HomeActions';
import ShoppingCartActions from '../../redux/actions/ShoppingCartActions';
import { tokenCopyPaste } from '../../redux/utils/functions'
import { images, API } from '../../redux/utils/constants';
import FlashMessage from '../../components/FlashMessage';

interface IProps {
  _get: any;
  _post: any;
  setData: any;
}

interface IState {
  data: any;
  ShoppingCart: any;
}
let propertyName = {} as any;
class App extends React.Component<IProps, IState> {

  constructor(props: any) {
    super(props)

    this.state = {
      data: props.data, ShoppingCart: props.shoppingCart
    };

  }
  componentDidMount() {
    setTimeout(() => {
      this.props.setData({ message: "Carrito Cargado", type: 'success', id: Math.random() }, 'message');
    }, 1000);
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {

    this.setState({
      ShoppingCart: nextProps.ShoppingCart
    });

  }
  handleChangeBurrencyBreakdownTotal() {

    /*let total_cash_submitted = 0;
    let total_secundary_submitted = 0;
    let open_amount = 0;

    this.state.total_cash_submitted_data.map((item, index) => {
      total_cash_submitted = total_cash_submitted + item.total;
    });

    this.state.total_secundary_submitted_data.map((item, index) => {
      total_secundary_submitted = total_secundary_submitted + item.total;
    });

    open_amount = parseFloat(parseFloat(total_cash_submitted) + (this.state.currencySecundary.operation == '/' ? parseFloat(total_secundary_submitted) * parseFloat(this.state.fields.tasa_cambio) : parseFloat(total_secundary_submitted) / parseFloat(this.state.fields.tasa_cambio))).toFixed(2);

    let fields = this.state.fields;
    fields['total_cash_submitted'] = total_cash_submitted;
    fields['total_secundary_submitted'] = total_secundary_submitted;
    fields['open_amount'] = open_amount;

    this.setState({
      fields: fields
    });*/

  }
  handleChange_input_price(e: any, row: any) {
    /*const price = e.target.value;
    this.setState(prevState => ({
      card_data: prevState.card_data.map(c => c.id === row.id ? Object.assign(c, { price_input_value: price }) : c)
    }));*/
  }
  handleChangeBurrencyBreakdown(e: any, row: any) {
    console.log(row);
    let row_tem: any = {};

    if (e.target.name == "btnAddCash") {
      console.log("agregando", (row.quantity + 1));
      const tem_quantity = row.quantity + 1;
      row_tem = {
        id: row.id,
        variation: row.variation,
        name: row.name,
        product_image: row.product_image,
        product_type: row.type,
        unit_price: parseFloat(row.unit_price),
        line_discount_type: 'fixed',
        line_discount_amount: 0.00,
        item_tax: 0.00,
        product_id: row.id,
        variation_id: row.variation_id,
        enable_stock: row.enable_stock,
        quantity: tem_quantity,
        product_unit_id: 'ojo',
        sub_unit_id: 'ojo',
        base_unit_multiplier: 'ojo',
        unit_price_inc_tax: parseFloat(row.unit_price_inc_tax),
        tax_id: null,
        sell_line_note: null,
        total: parseFloat(row.unit_price) * tem_quantity
      };

    } else if (e.target.name == "btnSubstractCash") {

      const tem_quantity = (row.quantity - 1) < 1 ? 1 : (row.quantity - 1);

      row_tem = {
        id: row.id,
        variation: row.variation,
        name: row.name,
        product_image: row.product_image,
        product_type: row.type,
        unit_price: parseFloat(row.unit_price),
        line_discount_type: 'fixed',
        line_discount_amount: 0.00,
        item_tax: 0.00,
        product_id: row.id,
        variation_id: row.variation_id,
        enable_stock: row.enable_stock,
        quantity: tem_quantity,
        product_unit_id: 'ojo',
        sub_unit_id: 'ojo',
        base_unit_multiplier: 'ojo',
        unit_price_inc_tax: parseFloat(row.unit_price_inc_tax),
        tax_id: null,
        sell_line_note: null,
        total: parseFloat(row.unit_price) * tem_quantity
      };

    }

    let tem_data_cart: any = [];
    for (const [index, value] of this.state.ShoppingCart.data_cart.entries()) {
      console.log(value.id, row.id);
      if (value.id == row.id) {
        tem_data_cart.push(row_tem);
      } else {
        tem_data_cart.push(value);
      }
    };
    console.log(tem_data_cart);
    this.props.setData(tem_data_cart, 'data_cart');

    setTimeout(() => {
      //this.handleChangeBurrencyBreakdownTotal();
    }, 100);

  }

  CarDeleteitem(e: any, row: any) {
    let tem_data_cart: any = [];
    for (const [index, value] of this.state.ShoppingCart.data_cart.entries()) {
      if (value.id != row.id) {
        tem_data_cart.push(value);
      };
    };
    this.props.setData(tem_data_cart, 'data_cart');
  }
  SendPurchaseOrder() {
    let dataerrors: any = [];
    let validation_fields = false;

    if (!this.state.ShoppingCart.fields.delivery_address) { dataerrors["delivery_address"] = ["*Campo requerido"]; validation_fields = true; };
    if (!this.state.ShoppingCart.fields.mobile) { dataerrors["mobile"] = ["*Campo requerido"]; validation_fields = true; };
    if (!this.state.ShoppingCart.fields.names) { dataerrors["names"] = ["*Campo requerido"]; validation_fields = true; };
    if (!this.state.ShoppingCart.fields.surnames) { dataerrors["surnames"] = ["*Campo requerido"]; validation_fields = true; };

    if (this.state.ShoppingCart.data_cart.length == 0) {
      validation_fields = true;
      this.props.setData({ message: "Agregar Productos Al Carrito", type: 'error', id: Math.random() }, 'message');

    }
    this.props.setData(dataerrors, 'errors');

    if (!validation_fields) {

      let post = {
        size: 'all',
        location_id: 1,
        contact_id: 1,
        price_group: 0,
        invoice_scheme_id: 1,
        sell_price_tax: 'includes',
        products: this.state.ShoppingCart.data_cart,
        discount_type: 'percentage',
        discount_amount: 0.00,
        rp_redeemed: 0,
        rp_redeemed_amount: 0,
        tax_calculation_amount: 0.00,
        advance_balance: 0.0000,
        payment: [
          {
            amount: 260.00,
            method: 'cash',
            card_type: 'credit'
          }
        ],
        change_return: 0.00,
        is_suspend: 0,
        recur_interval_type: 'days',
        is_credit_sale: 0,
        final_total: 260.00,
        discount_type_modal: 'percentage',
        discount_amount_modal: 0.00,
        shipping_charges_modal: 0,
        status: 'draft',
        tax_rate_id: null
      }
      this.props._post(post ,'ecommerce/create');

    } else {
      if (this.state.ShoppingCart.data_cart.length == 0) {

        this.props.setData({ message: "Agregar Productos Al Carrito", type: 'error', id: Math.random() }, 'message');

      } else {
        this.props.setData({ message: "LLenar Campos Requeridos", type: 'error', id: Math.random() }, 'message');

      }
    }
  }
  onChangeFields(e: any) {

    let fields = this.state.ShoppingCart.fields;
    fields[e.target.name] = e.target.value;
    this.props.setData(fields, 'fields');

  }
  render() {

    const { ShoppingCart } = this.state;
    let TotalCart: any = 0;

    if (ShoppingCart && ShoppingCart.message && ShoppingCart.message.id) {

      propertyName.handleShow(ShoppingCart.message);
      this.props.setData({}, 'message');

    }
    if (this.state.ShoppingCart && this.state.ShoppingCart.data_cart && this.state.ShoppingCart.data_cart) {

      this.state.ShoppingCart && this.state.ShoppingCart.data_cart && this.state.ShoppingCart.data_cart.map((val: any, i: any) =>
        TotalCart = (parseFloat(TotalCart) + parseFloat(val.total))
      )

    }
    return (
      <div className="row">
        <FlashMessage ref={ref => propertyName = ref} />

        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="table-responsive shopping-cart">
                  <table className="table mb-0">
                    <thead>
                      <tr>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th>Eliminar</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.ShoppingCart && this.state.ShoppingCart.data_cart && this.state.ShoppingCart.data_cart.map((val: any, i: any) =>
                          <tr>
                            <td>
                              <img src={val.product_image ? images + val.product_image : images + 'default.png'} alt="" height="52" />
                              <p className="d-inline-block align-middle mb-0">
                                <a href="" className="d-inline-block align-middle mb-0 product-name">{val.name}</a>
                                <br />
                                <span className="text-muted font-13">({val.variation})</span>
                              </p>
                            </td>
                            <td style={{ textAlign: "right" }}>{parseFloat(val.unit_price).toFixed(2)}</td>
                            <td>
                              <div className="col-md-6 form-inline justify-content-center" style={{ marginBottom: "15px" }}>
                                <div className="form-group">
                                  <div className="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                    <span className="input-group-btn input-group-prepend">
                                      <button className="btn btn-outline-primary waves-effect waves-light" type="button" name={'btnSubstractCash'} onClick={(e) => this.handleChangeBurrencyBreakdown(e, val)}>-</button>
                                    </span>
                                    <input id="demo2" type="text" style={{ maxWidth: "90px" }} value={val.quantity} onChange={(e) => this.handleChange_input_price(e, val)} className="form-control" />
                                    <span className="input-group-btn input-group-append">
                                      <button className="btn btn-outline-primary waves-effect waves-light" type="button" name={'btnAddCash'} onClick={(e) => this.handleChangeBurrencyBreakdown(e, val)}>+</button>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td style={{ textAlign: "right" }}>{parseFloat(val.total).toFixed(2)}</td>
                            <td>
                              <a onClick={(e) => this.CarDeleteitem(e, val)} className="text-dark"><i className="mdi mdi-close-circle-outline font-18"></i></a>
                            </td>
                          </tr>
                        )
                      }
                    </tbody>
                    <tfoot>
                      <tr>
                        <th colSpan={3} style={{ textAlign: "right" }}>Total:</th>
                        <th style={{ textAlign: "right" }}>
                          {TotalCart}
                        </th>
                        <th ></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="header-title mt-0 mb-3">Datos Generales Del Pedido</h4>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Nombres <small className="text-danger font-13">*</small></label>
                    <input onChange={this.onChangeFields.bind(this)} type="text" className="form-control" name="names" value={ShoppingCart && ShoppingCart.fields && ShoppingCart.fields.names} />
                    {ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.names && <div className="invalid-feedback" style={{ display: "block" }}>{ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.names}</div>}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Apellidos <small className="text-danger font-13">*</small></label>
                    <input onChange={this.onChangeFields.bind(this)} type="text" className="form-control" name="surnames" value={ShoppingCart && ShoppingCart.fields && ShoppingCart.fields.surnames} />
                    {ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.surnames && <div className="invalid-feedback" style={{ display: "block" }}>{ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.surnames}</div>}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <div className="form-group">
                    <label>Celular <small className="text-danger font-13">*</small></label>
                    <input onChange={this.onChangeFields.bind(this)} type="text" className="form-control" name="mobile" value={ShoppingCart && ShoppingCart.fields && ShoppingCart.fields.mobile} />
                    {ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.mobile && <div className="invalid-feedback" style={{ display: "block" }}>{ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.mobile}</div>}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <label>Dirección de Entrega <small className="text-danger font-13">*</small></label>
                    <input onChange={this.onChangeFields.bind(this)} type="text" className="form-control" name="delivery_address" value={ShoppingCart && ShoppingCart.fields && ShoppingCart.fields.delivery_address} />
                    {ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.delivery_address && <div className="invalid-feedback" style={{ display: "block" }}>{ShoppingCart && ShoppingCart.fields && ShoppingCart.errors.delivery_address}</div>}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12" style={{ textAlign: "right" }}>
                  <button type="button" onClick={this.SendPurchaseOrder.bind(this)} className="btn btn-outline-success btn-round waves-effect waves-light"><i className="mdi mdi-email-outline mr-2"></i>Enviar Pedido</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state: any) => {
  return {
    ShoppingCart: state.ShoppingCart
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    setData: (item: any, name: string) => dispatch(ShoppingCartActions.setData(item, name)),
    _post: (fields: any, controller: string) => dispatch(ShoppingCartActions._post(fields, controller)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(App);
