import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import HomeActions from '../../redux/actions/HomeActions';
import ShoppingCartActions from '../../redux/actions/ShoppingCartActions';
import { tokenCopyPaste } from '../../redux/utils/functions'
import { images, API } from '../../redux/utils/constants';
import FlashMessage from '../../components/FlashMessage';

interface IProps {
  _get: any;
  _post: any;
  setData: any;
}

interface IState {
  data: any;
  data_products: any;
  data_categories: any;
  total: any;
  per_page: any;
  current_page: any;
  aux: any;
  category_id: any;
  data_cart: any;
  ShoppingCart: any;
  message: any;
}
let propertyName = {} as any;
class App extends React.Component<IProps, IState> {

  constructor(props: any) {
    super(props)

    this.state = {
      data: props.data,
      data_products: props.data_products,
      data_categories: props.data_categories,
      total: props.total,
      per_page: props.per_page,
      current_page: props.current_page,
      aux: props.aux,
      category_id: props.category_id,
      data_cart: props.data_cart,
      ShoppingCart: props.ShoppingCart,
      message: props.message

    };

  }
  componentDidMount() {
    this.setState({
      category_id: 'all'
    });
    setTimeout(() => {
      this.makeHttpRequestWithPage(1);
    }, 100);

    this.props._get({ typeOption: 'get-data' }, 'ecommerce/get-data');

    let post = {
      size: 'all',
      location_id: 1,
      contact_id: 1,
      price_group: 0,
      invoice_scheme_id: 1,
      sell_price_tax: 'includes',
      products: [
        {
          product_type: 'variable',
          unit_price: 130.00,
          line_discount_type: 'fixed',
          line_discount_amount: 0.00,
          item_tax: 0.00,
          product_id: 13,
          variation_id: 38,
          enable_stock: 1,
          quantity: 1.00,
          product_unit_id: 1,
          sub_unit_id: 1,
          base_unit_multiplier: 1,
          unit_price_inc_tax: 130.00,
          tax_id: null,
          sell_line_note: null
        },
        {
          product_type: 'variable',
          unit_price: 130.00,
          line_discount_type: 'fixed',
          line_discount_amount: 0.00,
          item_tax: 0.00,
          product_id: 13,
          variation_id: 39,
          enable_stock: 1,
          quantity: 1.00,
          product_unit_id: 1,
          sub_unit_id: 1,
          base_unit_multiplier: 1,
          unit_price_inc_tax: 130.00,
          tax_id: null,
          sell_line_note: null
        }
      ],
      discount_type: 'percentage',
      discount_amount: 0.00,
      rp_redeemed: 0,
      rp_redeemed_amount: 0,
      tax_calculation_amount: 0.00,
      advance_balance: 0.0000,
      payment: [
        {
          amount: 260.00,
          method: 'cash',
          card_type: 'credit'
        }
      ],
      change_return: 0.00,
      is_suspend: 0,
      recur_interval_type: 'days',
      is_credit_sale: 0,
      final_total: 260.00,
      discount_type_modal: 'percentage',
      discount_amount_modal: 0.00,
      shipping_charges_modal: 0,
      status: 'draft',
      tax_rate_id: null
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps: any) {

    let data_categories_temp: any = [];

    nextProps.home.categories.map((row: any, index: any) => (
      data_categories_temp.push({ id: row.id, categorie: row.name, expanded: false, sub_categorie: row.sub_categories ? row.sub_categories : null })
    ));

    this.setState({
      data_categories: data_categories_temp,
      ShoppingCart: nextProps.ShoppingCart
    });

  }
  renderProducts(row: any, index: any) {
    let img = row.product_image ? images + row.product_image : images + 'default.png';
    return (
      <div className="col-lg-3">
        <div className="card e-co-product" style={{ border: "1px solid #eff2f9" }}>
          <a>
            <img src={img} alt="" className="img-fluid" />
          </a>
          <div className="card-body product-info">
            <a className="product-title">{row.name} ({row.variation})</a>
            <div className="d-flex justify-content-between my-2">
              <p className="product-price">{row.selling_price} <span className="ml-2"></span></p>
            </div>
            <button className="btn btn-soft-pink btn-round waves-effect waves-light" onClick={this.ProductToCar.bind(this, row, index)} ><i className="mdi mdi-cart mr-1"></i> Add To Cart</button>
            <button className="btn btn-twitter btn-round" data-toggle="tooltip" data-placement="top" title="Quickview" onClick={this.modalCar.bind(this, row)}><i className="mdi mdi-magnify"></i></button>
          </div>
        </div>
      </div>
    );
  }
  modalCar(row: any) {

  }

  CategorieMenuActive(id: any) {

    const temp_data_categories = [];
    for (const [index, value] of this.state.data_categories.entries()) {
      if (value.id == id) {
        const temp_sub_categories = [];

        if (value.sub_categorie != null) {
          for (const [index_, value_] of value.sub_categorie.entries()) {
            temp_sub_categories.push(
              {
                id: value_.id,
                name: value_.name,
                active: false,
              }
            );
          }
        };
        temp_data_categories.push(
          {
            id: value.id,
            categorie: value.categorie,
            expanded: true,
            sub_categorie: temp_sub_categories
          }
        );
        this.setState({
          category_id: value.id
        });
        setTimeout(() => {
          this.makeHttpRequestWithPage(1);
        }, 100);
      } else {
        const temp_sub_categories = [];
        if (value.sub_categorie != null) {
          for (const [index_, value_] of value.sub_categorie.entries()) {
            temp_sub_categories.push(
              {
                id: value_.id,
                name: value_.name,
                active: false,
              }
            );
          }

        };
        temp_data_categories.push(
          {
            id: value.id,
            categorie: value.categorie,
            expanded: false,
            sub_categorie: temp_sub_categories
          }
        );

      }
    }
    this.setState({
      data_categories: temp_data_categories
    });

  }
  CategorieSubMenuActive(menu: any, submenu: any) {

    const temp_data_categories = [];
    let temp_category_id = 'all';

    for (const [index, value] of this.state.data_categories.entries()) {
      if (value.id == menu) {
        const temp_sub_categories = [];
        for (const [index_, value_] of value.sub_categorie.entries()) {
          if (value_.id == submenu) {
            temp_category_id = value_.id;
            temp_sub_categories.push(
              {
                id: value_.id,
                name: value_.name,
                active: true,
              }
            );
            this.setState({
              category_id: temp_category_id
            });
            setTimeout(() => {
              this.makeHttpRequestWithPage(1);
            }, 100);
          } else {
            temp_sub_categories.push(
              {
                id: value_.id,
                name: value_.name,
                active: false,
              }
            );
          }
        }
        temp_data_categories.push(
          {
            id: value.id,
            categorie: value.categorie,
            expanded: value.expanded,
            sub_categorie: temp_sub_categories
          }
        );
      } else {
        temp_data_categories.push(
          {
            id: value.id,
            categorie: value.categorie,
            expanded: false,
            sub_categorie: value.sub_categorie
          }
        );
      }
    }

    this.setState({
      data_categories: temp_data_categories
    });
  }
  makeHttpRequestWithPage = async (pageNumber: any) => {
    const response = await fetch(`${API}ecommerce?typeOption=get_proyects_cart&page=${pageNumber}&limit=${12}&search=${''}&category_id=${this.state.category_id}&brand_id=all&location_id=1&is_enabled_stock=0&repair_model_id=0`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const data = await response.json();

    this.setState({
      data_products: data.data.data,
      total: data.data.total,
      per_page: data.data.per_page,
      current_page: data.data.current_page
    });

  }
  ProductToCar(row: any, index: any) {

    let ShoppingCart_temp = this.state.ShoppingCart.data_cart;

    let FilterShoppingCart: any = this.state.ShoppingCart.data_cart.filter((c: any) => c.id == row.id);

    //if (FilterShoppingCart.length == 0) {
    ShoppingCart_temp.push({
      id: row.product_id,
      variation: row.variation,
      name: row.name,
      product_image: row.product_image,
      product_type: row.type,
      unit_price: parseFloat(row.selling_price),
      line_discount_type: 'fixed',
      line_discount_amount: 0.00,
      item_tax: 0.00,
      product_id: row.product_id,
      variation_id: row.variation_id,
      enable_stock: row.enable_stock,
      quantity: 1,
      product_unit_id: 'ojo',
      sub_unit_id: 'ojo',
      base_unit_multiplier: 'ojo',
      unit_price_inc_tax: parseFloat(row.selling_price),
      tax_id: null,
      sell_line_note: null,
      total: parseFloat(row.selling_price)
    });
    this.props.setData(ShoppingCart_temp, 'data_cart');
    /*} else {
      this.props.setData({ message: "El producto ya fue agregado", type: 'success', id: Math.random() }, 'message');
    }*/


  }
  render() {

    const { data_products, total, per_page, current_page, message } = this.state;
    let data_products_render = [];
    let courses_items_ = 1;

    if (data_products && courses_items_ == 1) {

      data_products_render = data_products.map((val: any, i: any) => (
        this.renderProducts(val, i)
      ));

      courses_items_ = 2;

    }

    let renderPageNumbers;
    const pageNumbers = [];

    if (total !== null) {
      for (let i = 1; i <= Math.ceil(total / per_page); i++) {
        pageNumbers.push(i);
      }

      var vm = this;
      renderPageNumbers = pageNumbers.map(number => {
        let classes = current_page === number ? "page-item active" : 'page-item';
        if (number == 1 || number == total || (number >= current_page - 2 && number <= current_page + 2)) {
          return (
            <li key={number} className={classes} style={{ cursor: "pointer" }}><a className="page-link" onClick={() => this.makeHttpRequestWithPage(number)}>{number}</a></li>
          );
        }
      });
    }

    if (this.state.ShoppingCart.message && this.state.ShoppingCart.message.id) {

      propertyName.handleShow(this.state.ShoppingCart.message);

    }
    //this.FlashMessage.current?.handleShow('sasasas');
    return (
      <div className="row">

        <FlashMessage ref={ref => propertyName = ref} />

        <div className="col-md-3">
          <div className="card">
            <div className="card-body">

              <ul className="metismenu left-sidenav-menu">
                {
                  this.state.data_categories && this.state.data_categories.map((val: any, i: any) =>
                    <li className={val.expanded ? 'mm-active' : ''}>
                      <a onClick={this.CategorieMenuActive.bind(this, val.id)} style={{ cursor: "pointer" }}><i className="ti-bar-chart"></i><span>{val.categorie}</span><span className="menu-arrow">{val.sub_categorie != null ? <i className="mdi mdi-chevron-right"></i> : null}   </span></a>
                      {
                        val.expanded ?
                          <ul className="nav-second-level" aria-expanded="false">
                            {
                              val.sub_categorie && val.sub_categorie.map((val_: any, i_: any) =>
                                <li className={val_.active ? 'nav-item active' : ''}><a onClick={this.CategorieSubMenuActive.bind(this, val.id, val_.id)} className={val_.active ? 'nav-link active' : ''}><i className="ti-control-record"></i>{val_.name}</a></li>
                              )
                            }

                          </ul>
                          :
                          null
                      }
                    </li>
                  )
                }
              </ul>

            </div>
          </div>
        </div>
        <div className="col-md-9">
          <div className="card">
            <div className="card-body">
              <div className="row">
                {data_products_render}
              </div>

            </div>
          </div>
          <div className="col-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <nav aria-label="Page navigation example" style={{ float: 'left' }}>
                      <ul className="pagination">
                        Showing {per_page} to {current_page} of {total} entries
                      </ul>
                    </nav>
                    <nav aria-label="Page navigation example" style={{ float: 'right' }}>
                      <ul className="pagination">
                        <li className="page-item" style={{ cursor: "pointer" }}><a className="page-link" onClick={() => this.makeHttpRequestWithPage(1)}>First</a></li>
                        <li className={current_page == 1 ? "page-item disabled" : "page-item"} style={current_page == 1 ? {} : { cursor: "pointer" }} ><a className="page-link" onClick={() => this.makeHttpRequestWithPage(current_page - 1)}>Previous</a></li>
                        {renderPageNumbers}
                        <li className={current_page == total ? "page-item disabled" : "page-item"} style={current_page == total ? {} : { cursor: "pointer" }}><a className="page-link" onClick={() => this.makeHttpRequestWithPage(current_page + 1)}>Next</a></li>
                        <li className="page-item" style={{ cursor: "pointer" }}><a className="page-link" onClick={() => this.makeHttpRequestWithPage(total)}>Last</a></li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  };
}

const mapStateToProps = (state: any) => {
  return {
    _get: PropTypes.func.isRequired,
    home: state.home,
    ShoppingCart: state.ShoppingCart,
    _post: PropTypes.func.isRequired,
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    _get: (fields: any, controller: string) => dispatch(HomeActions._get(fields, controller)),
    _post: (fields: any, controller: string) => dispatch(HomeActions._post(fields, controller)),
    setData: (item: any, name: string) => dispatch(ShoppingCartActions.setData(item, name))
  }
};
//export default connect(mapStateToProps, mapDispatchToProps)(App);
export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(App);
