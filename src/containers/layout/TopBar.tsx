import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import {
    BrowserRouter as Router,
    NavLink,
    Link,
    Switch,
    Route
} from "react-router-dom";
import { images, API } from '../../redux/utils/constants';
interface IProps {
}

interface IState {
    data: any;
    ShoppingCart: any;
}

class TopBar extends React.Component<IProps, IState> {
    constructor(props: any) {
        super(props)
        this.state = { data: props.data, ShoppingCart: props.ShoppingCart, }
    }

    componentDidMount() {

    }
    UNSAFE_componentWillReceiveProps(nextProps: any) {

        this.setState({
          ShoppingCart: nextProps.ShoppingCart
        });
    
      }

    render() {
        const { ShoppingCart } = this.state;
        let subtotal_card: any = 0;
        this.state.ShoppingCart.data_cart.map((val: any, i: any) => (
            subtotal_card = parseFloat(subtotal_card) + parseFloat(val.selling_price)
        ));
        return (
            <div className="topbar">
                <nav className="topbar-main">
                    <div className="topbar-left">
                        <a href="../projects/projects-index.html" className="logo">
                            <span>
                                <img src="./images/logo-sm.png" alt="logo-small" className="logo-sm" />
                            </span>
                            <span>
                                <img src="./images/logo-dark.png" alt="logo-large" className="logo-lg" />
                            </span>
                        </a>
                    </div>
                    <ul className="list-unstyled topbar-nav float-right mb-0">
                        <li className="hidden-sm">
                            <a className="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                English <img src="./images/flags/us_flag.jpg" className="ml-2" height="16" alt="" /> <i className="mdi mdi-chevron-down"></i>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right">
                                <a className="dropdown-item" href="#"><span> German </span><img src="./images/flags/germany_flag.jpg" alt="" className="ml-2 float-right" height="14" /></a>
                                <a className="dropdown-item" href="#"><span> Italian </span><img src="./images/flags/italy_flag.jpg" alt="" className="ml-2 float-right" height="14" /></a>
                                <a className="dropdown-item" href="#"><span> French </span><img src="./images/flags/french_flag.jpg" alt="" className="ml-2 float-right" height="14" /></a>
                                <a className="dropdown-item" href="#"><span> Spanish </span><img src="./images/flags/spain_flag.jpg" alt="" className="ml-2 float-right" height="14" /></a>
                                <a className="dropdown-item" href="#"><span> Russian </span><img src="./images/flags/russia_flag.jpg" alt="" className="ml-2 float-right" height="14" /></a>
                            </div>
                        </li>

                        <li className="dropdown notification-list">
                            <a className="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                <i className="dripicons-bell noti-icon"></i>
                                <span className="badge badge-danger badge-pill noti-icon-badge">{this.state.ShoppingCart.data_cart.length}</span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right" style={{ width: '460px', height: '500px' }}>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="card">
                                            <div className="card-body">
                                                <h4 className="header-title mt-0 mb-3">Orden de Compra</h4>
                                                <div className="table-responsive shopping-cart " style={{ height: '450px' }}>
                                                    <table className="table mb-0">
                                                        <thead>
                                                            <tr>
                                                                <th>Product</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                this.state.ShoppingCart.data_cart && this.state.ShoppingCart.data_cart.map((val: any, i: any) =>
                                                                    <tr>
                                                                        <td>
                                                                            <img src={val.product_image ? images + val.product_image : images + 'default.png'} alt="" height="52" />
                                                                            <p className="d-inline-block align-middle mb-0 product-name">{val.name} ({val.variation})</p>
                                                                        </td>
                                                                        <td>{val.selling_price}</td>
                                                                    </tr>
                                                                )

                                                            }
                                                            <tr>
                                                                <td>
                                                                    Sub-Total
                                                                </td>
                                                                <td>{subtotal_card}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="button-items">
                                            <button type="button" className="btn btn-dark btn-lg btn-block">Comenzar Pedido</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </li>

                        <li className="dropdown">
                            <a className="nav-link dropdown-toggle waves-effect waves-light nav-user pr-0" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                <img src="./images/users/user-4.jpg" alt="profile-user" className="rounded-circle" />
                                <span className="ml-1 nav-user-name hidden-sm">Amelia <i className="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right">
                                <a className="dropdown-item" href="#"><i className="dripicons-user text-muted mr-2"></i> Profile</a>
                                <a className="dropdown-item" href="#"><i className="dripicons-wallet text-muted mr-2"></i> My Wallet</a>
                                <a className="dropdown-item" href="#"><i className="dripicons-gear text-muted mr-2"></i> Settings</a>
                                <a className="dropdown-item" href="#"><i className="dripicons-lock text-muted mr-2"></i> Lock screen</a>
                                <div className="dropdown-divider"></div>
                                <a className="dropdown-item" href="#"><i className="dripicons-exit text-muted mr-2"></i> Logout</a>
                            </div>
                        </li>
                        <li className="menu-item">
                            <a className="navbar-toggle nav-link" id="mobileToggle">
                                <div className="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <ul className="list-unstyled topbar-nav mb-0">
                        <li className="hide-phone app-search">
                            <form role="search" className="">
                                <input type="text" placeholder="Search..." className="form-control" />
                                <a href=""><i className="fas fa-search"></i></a>
                            </form>
                        </li>
                    </ul>
                </nav>
                <div className="navbar-custom-menu">
                    <div className="container-fluid">
                        <div id="navigation">
                            <ul className="navigation-menu">
                                <li className="has-submenu active last-elements">
                                    <a href="#" onClick={e => e.preventDefault()} className="navbar-link">
                                        <i className="ti-bar-chart"></i>
                                        <span>Analytics</span>
                                    </a>
                                    <ul className="submenu submenu-tab  show">
                                        <li><a href="#"><i className="ti-desktop"></i>Dashboard</a></li>
                                        <li><a href="#"><i className="ti-id-badge"></i>Customers</a></li>
                                        <li><a href="#"><i className="ti-receipt"></i>Reports</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

const mapStateToProps = (state: any) => {
    return {
        ShoppingCart: state.ShoppingCart
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TopBar);
