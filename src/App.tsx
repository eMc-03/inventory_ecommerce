import React from 'react';
import {
  BrowserRouter as Router,
  NavLink,
  Link,
  Switch,
  Route
} from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Home from './containers/home';
import Error from './containers/404';
import PurchaseOrder from './containers/PurchaseOrder';
import TopBar from './containers/layout/TopBar';
import { images, API ,ROOT } from './redux/utils/constants';

function App() {
  return (
    <div className="App">
      <TopBar />
      <div className="page-wrapper">
        <div className="page-content">
          <div className="container-fluid">
            <Router>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/error">User</Link>
                </li>
                <li>
                  <Link to="/purchaseorder">Orden de Compra</Link>
                </li>
              </ul>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/error" component={Error} />
                <Route path="/purchaseorder" component={PurchaseOrder} />
              </Switch>
            </Router>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
