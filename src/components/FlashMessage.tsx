import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
//import 'animate.css';
import ReactNotifications from 'react-notifications-component';

interface IProps {
}

interface IState {
    data: any;
}

class FlashMessage extends React.Component<IProps, IState>{
    constructor(props: any) {
        super(props);
        this.state = {
            data: props.data
        };
    }
    componentDidMount() {
        this.setState({
            data: []
        });

    }
    handleShow(message: any): void {
   
       // if (typeof this.state.data != "undefined" &&this.state.data.id != message.id) {
            switch (message.type) {
                case "success":
                    store.addNotification({
                        title: 'Success',
                        message: message.message,
                        type: 'success',
                        container: 'top-right',
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        //showIcon: true,
                        dismiss: {
                            duration: 5000,
                            showIcon: true,
                            onScreen: true,
                            pauseOnHover: true,
                            //dismissIcon: true
                        }
                    });
                    break;
                case "warning":
                    store.addNotification({
                        title: 'Warning',
                        message: message.message,
                        type: 'warning',
                        container: 'top-right',
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        //showIcon: true,
                        dismiss: {
                            duration: 5000,
                            showIcon: true,
                            onScreen: true,
                            pauseOnHover: true,
                            // dismissIcon: true
                        }
                    });
                    break;
                case "error":
                    store.addNotification({
                        title: 'Danger',
                        message: message.message,
                        type: 'danger',
                        container: 'top-right',
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        //showIcon: true,
                        dismiss: {
                            duration: 5000,
                            showIcon: true,
                            onScreen: true,
                            pauseOnHover: true,
                            //dismissIcon: true
                        }
                    });
                    break;
                default:
                    store.addNotification({
                        title: 'Info',
                        message: message.message,
                        type: 'info',
                        container: 'top-right',
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        //showIcon: true,
                        dismiss: {
                            duration: 5000,
                            showIcon: true,
                            onScreen: true,
                            pauseOnHover: true,
                            //dismissIcon: true
                        }
                    });
                    break;
            }
       /* };

        this.setState({

            data: message

        });*/

        //this.state.data = message;

    }
    render() {
        return (
            <ReactNotifications />
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(FlashMessage);